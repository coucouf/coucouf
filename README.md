Core maintainer of the [KDE stack](https://qa.debian.org/developer.php?login=debian-qt-kde%40lists.debian.org).

Main contributor on the [`desktop-base`](https://tracker.debian.org/pkg/desktop-base) package setting up various desktop environments’ defaults and Debian chosen wallpaper. 


<p>
Mastodon:
<a rel="me" href="https://framapiaf.org/@coucouf">@coucouf@framapiaf.org</a>
</p>
